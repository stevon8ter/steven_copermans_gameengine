https://bitbucket.org/stevon8ter/steven_copermans_gameengine/src/master/

Design Choices:
	- templated ServiceLocator for all important services (sceneManager, ResourceManager, Renderer, ...)
		- This way there's no need to inject the dependencies in all objects
		- I did the same thing for components in gameObjects, this way you can retrieve them using the GetComponent<T>() function
		- Services get called using _globalVariableOfTheServiceLocator.GetService<ServiceYouDesire>();
		
	- The game loop will not continue until a certain delay between the last update and the current state has occured, this limits the FPS to a max, but does not reduce the update when there's alot going on (it is to say, it will reduce, but will not wait longer because more than enough time has passed already)
	
	- Components
		- Took a look at how unity does it
		- Every component has a Transform, and each GameObject has a "parent" transform as well
	
	- Commands
		- You can define inputs, and give them commands, as long as you pass a "KeyLibrary" to it, this is to make sure you can use the same call function for keyboard and controller
		- Keyboard key would be "UP" -> SDL_SCANCODE_UP
		- Controller key would be "UP" -> XINPUT_DPAD_UP or so