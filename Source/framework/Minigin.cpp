#include "MiniginPCH.h"
#include "Project_Selector.h"

// ReSharper disable once CppUnusedIncludeDirective
#include <vld.h>
#pragma comment(lib,"xinput.lib")
#include "SDL.h"
#include <chrono>
#include <thread>

#include "Services.h"
#include "IApp.h"
#include "Transform.h"
#include "GameObject.h"
#include "Components.h"

#ifdef Game
#include Game
#endif
const float msPerFrame = 1000 / 60; //16 for 60 fps, 33 for 30 fps
float timeSinceLastUpdate = msPerFrame;


#ifdef Game
#ifdef Game_Object
Game_Object *_game = new Game_Object();
#endif
#endif

void Initialize();
void LoadGame();
void Cleanup();

#pragma warning( push )  
#pragma warning( disable : 4100 )  
int main(int argc, char* argv[]) {
#pragma warning( pop )

	
	Initialize();
	LoadGame();

	{
		auto renderer = _serviceLocator.GetService<RenderService>();
		auto sceneManager = _serviceLocator.GetService<SceneService>();
		auto input = _serviceLocator.GetService<InputService>();

		bool doContinue = true;
		while(doContinue) 
		{
			auto t = std::chrono::high_resolution_clock::now();


			input->HandleInputs();
			doContinue = !input->ShouldQuit();

			sceneManager->Update();
			renderer->Render();

			#ifdef Game
			#ifdef Game_Object
						_game->Update();
			#endif
			#endif

			//auto go = sceneManager->GetActiveScene().GetGameObject("LeftPointer");
			//auto txtCmp = go->GetComponent<TextComponent>();
			//
			//Transform transform;
			//transform.position.x = txtCmp->GetSize().x + 75;
			//transform.position.y = 500;
			//
			//go->SetTransform(transform);

			auto t2 = std::chrono::high_resolution_clock::now();
			while (std::chrono::duration_cast<std::chrono::duration<float>>(t2 - t).count() < msPerFrame / 1000)
			{
				t2 = std::chrono::high_resolution_clock::now();
			}
			std::cout << std::chrono::duration_cast<std::chrono::duration<float>>(t2 - t).count() << std::endl;
		}
	}

	Cleanup();
    return 0;
}

SDL_Window* window;

void Initialize()
{
	std::string GameTitle = "No game was Selected";

	#ifdef Game
		GameTitle = Game;
	#endif

	auto pos = GameTitle.find("/");
	GameTitle = GameTitle.substr(0, pos);

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::stringstream ss; ss << "SDL_Init Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	window = SDL_CreateWindow(
		GameTitle.c_str(),
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		1000,                    
		750,                    
		SDL_WINDOW_OPENGL
	);
	if (window == nullptr) {
		std::stringstream ss; ss << "SDL_CreateWindow Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
	
	// Add Renderer and give it the window
	_serviceLocator.AddService(new RenderService(window));

	// Add SceneManager
	_serviceLocator.AddService(new SceneService());

	// Add resource manager with a path to the data
	_serviceLocator.AddService(new ResourceService("../Data/"));

	// Add Input manager
	_serviceLocator.AddService(new InputService());


}

void LoadGame()
{
#ifdef Game
#ifdef Game_Object
	_game->Start();
#endif
#endif
}

void Cleanup()
{
	delete _game;
	_serviceLocator.Destroy();
	SDL_DestroyWindow(window);
	SDL_Quit();
}