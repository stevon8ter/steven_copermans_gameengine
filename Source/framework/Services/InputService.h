#pragma once
#include <XInput.h>
#include "SDL.h"
#include "Input.h"

enum class ControllerButton
{
	ButtonA,
	ButtonB,
	ButtonX,
	ButtonY
};

class InputService final : public Service
{
public:
	InputService();
	~InputService();
	bool ShouldQuit();
	bool HandleInputs() const;
	void AddInput(std::shared_ptr<Input> input) { m_Inputs.push_back(input); input->SetEventPointer(m_Event); };
	void RemoveInput(int id);
	std::vector<std::shared_ptr<Input>> GetInputs() { return m_Inputs; };

protected:
	void Destroy() override;

private:
	XINPUT_STATE currentState{};
	std::vector<std::shared_ptr<Input>> m_Inputs;
	bool m_Quit = false;
	SDL_Event* m_Event = new SDL_Event();
};

