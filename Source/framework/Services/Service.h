#pragma once

class Service {
	friend class ServiceLocator;
public:
	~Service() { };
protected:
	virtual void Destroy() = 0;
	Service() {};

private:

};