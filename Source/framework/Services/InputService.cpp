#include "MiniginPCH.h"
#include "Services.h"
#include <algorithm>

InputService::InputService()
{
}

InputService::~InputService()
{
}

bool InputService::ShouldQuit()
{
	while (SDL_PollEvent(m_Event)) {
		if ((*m_Event).type == SDL_QUIT)
			return true;
	}

	return m_Quit;
}

bool InputService::HandleInputs() const
{
	for (auto input : m_Inputs)
	{
		input->HandleInput();
	}

	return false;
}

void InputService::RemoveInput(int id)
{
	auto it = std::find_if(m_Inputs.begin(), m_Inputs.end(), [&](std::shared_ptr<Input> input) -> bool { return input->GetControllerID() == id; });

	if (it != m_Inputs.end())
		m_Inputs.erase(it);
}

void InputService::Destroy()
{
	delete m_Event;
	delete this;
}
