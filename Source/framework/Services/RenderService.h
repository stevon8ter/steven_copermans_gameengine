#pragma once
#include <SDL.h>

struct SDL_Window;
struct SDL_Renderer;


class Texture2D;
class RenderService final : public Service
{
public:
	RenderService(SDL_Window* window);
	~RenderService();

	void Render();

	void RenderTexture(SDL_Texture* texture, float x, float y) const;
	void RenderTexture(SDL_Texture* texture, float x, float y, float width, float height) const;

	SDL_Renderer* GetSDLRenderer() const { return mRenderer; }

protected:
	void Destroy() override;

private:
	SDL_Renderer* mRenderer = nullptr;
};

