#include "MiniginPCH.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "Services.h"

ResourceService::ResourceService(std::string&& dataPath)
{
	m_DataPath = std::move(dataPath);

	// load support for png and jpg, this takes a while!

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
		std::stringstream ss; ss << "Failed to load support for png: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG) {
		std::stringstream ss; ss << "Failed to load support for jpg: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	if (TTF_Init() != 0) {
		std::stringstream ss; ss << "Failed to load support for fonts: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
}

ResourceService::~ResourceService()
{
}

SDL_Texture* ResourceService::CreateTextureFromFile(const std::string& file)
{
	std::string fullPath = (m_DataPath + file).c_str();
	//SDL_Texture *texture = IMG_LoadTexture(_serviceLocator.GetService<RenderService>()->GetSDLRenderer(), fullPath.c_str());

	const auto surface = IMG_Load(fullPath.c_str());
	if (surface == nullptr)
	{
		std::stringstream ss; ss << "Render Image failed: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	return CreateTextureFromSurface(surface);
}

SDL_Texture * ResourceService::CreateTextureFromString(const std::string & text, const std::string & fontPath, int ptSize, const SDL_Color & textColor)
{	
	TTF_Font* font = LoadFont(fontPath, ptSize);
	const auto surface = TTF_RenderText_Blended(font, text.c_str(), textColor);
	if (surface == nullptr) {
		std::stringstream ss; ss << "Render text failed: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
	TTF_CloseFont(font);
	return CreateTextureFromSurface(surface);
}

TTF_Font* ResourceService::LoadFont(const std::string& file, unsigned int size)
{
	std::string fullPath = m_DataPath + file;

	auto font = TTF_OpenFont(fullPath.c_str(), size);
	if (font == nullptr) {
		std::stringstream ss; ss << "Failed to load font: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	return font;
}

void ResourceService::Destroy()
{
	delete this;
}

SDL_Texture * ResourceService::CreateTextureFromSurface(SDL_Surface * surface)
{
	auto texture = SDL_CreateTextureFromSurface(_serviceLocator.GetService<RenderService>()->GetSDLRenderer(), surface);
	if (texture == nullptr) {
		std::stringstream ss; ss << "Create text texture from surface failed: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
	SDL_FreeSurface(surface);

	return texture;
}
