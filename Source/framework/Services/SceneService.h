#pragma once

class Scene;
class SceneService final : public Service
{
public:
	SceneService();
	~SceneService();

	Scene & CreateScene(const std::string& name);

	void Update();
	void Render();
	Scene& GetActiveScene();
	void SetActiveScene(const std::string& sceneName);

protected:
	void Destroy() override;

private:
	void ChangeScene();
	std::map<std::string, Scene*> m_Scenes;
	std::string m_ActiveScene;
	bool changeScene = false;
	Scene* nextScene;
};