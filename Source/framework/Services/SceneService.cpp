#include "MiniginPCH.h"
#include "Services.h"
#include "Scene.h"

SceneService::SceneService()
{
}

SceneService::~SceneService()
{
}

Scene & SceneService::CreateScene(const std::string & name)
{
	Scene* scene = new Scene(name);
	m_Scenes.insert(std::pair<std::string, Scene*>(name, scene));

	return *scene;
}

void SceneService::Update()
{
	if (changeScene)
	{
		ChangeScene();
	}
	if (m_Scenes.count(m_ActiveScene))
		m_Scenes[m_ActiveScene]->Update();
}

void SceneService::Render()
{
	if (m_Scenes.count(m_ActiveScene))
		m_Scenes[m_ActiveScene]->Render();
}

Scene & SceneService::GetActiveScene()
{
	if (m_Scenes.count(m_ActiveScene) == 0 && nextScene)
		return *nextScene;

	return *m_Scenes[m_ActiveScene];
}

void SceneService::SetActiveScene(const std::string & sceneName)
{
	if (m_Scenes.count(sceneName))
	{
		changeScene = true;
		nextScene = m_Scenes[sceneName];
	}
}

void SceneService::Destroy()
{
	for (auto scene : m_Scenes)
	{
		scene.second->Destroy();
	}
	delete this;
}

void SceneService::ChangeScene()
{
	if (m_ActiveScene != "")
	{
		std::cout << m_ActiveScene << std::endl;
		std::cout << "Going to exit scene: " << GetActiveScene().GetName() << std::endl;
		m_Scenes[m_ActiveScene]->Exit(*nextScene);
	}
	m_ActiveScene = nextScene->GetName();
	m_Scenes[m_ActiveScene]->Load();
	changeScene = false;

	nextScene = nullptr;
}
