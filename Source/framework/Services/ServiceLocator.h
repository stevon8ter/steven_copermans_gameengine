#pragma once
#include <map>
#include "Service.h"

class ServiceLocator
{
public:
	ServiceLocator() {};
	~ServiceLocator() { };

	void Destroy()
	{
		for (std::pair<char const * __ptr64, Service*> pair : m_Services)
		{	
			pair.second->Destroy();
		}
	}

	template<typename T>
	inline void AddService(T* service);

	template<typename T>
	inline T* GetService();

private:
	std::map<char const * __ptr64, Service*> m_Services;
};

template<typename T>
inline T* ServiceLocator::GetService()
{
	static_assert(std::is_base_of<Service, T>::value, "T must inherit from Service");
	auto it = m_Services.find(typeid(T).name());
	if (it != m_Services.end())
	{
		return static_cast<T *>((*it).second);
	}
		
	return nullptr;
}

template<typename T>
inline void ServiceLocator::AddService(T * service)
{
	static_assert(std::is_base_of<Service, T>::value, "T must inherit from Service");
	m_Services.insert(std::pair<char const * __ptr64, Service*>(typeid(T).name(), service));
}
