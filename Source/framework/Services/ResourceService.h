#pragma once
#include <SDL.h>
#include <SDL_ttf.h>

class Font;
class ResourceService final : public Service
{
public:
	ResourceService(std::string&& dataPath);
	~ResourceService();

	SDL_Texture* CreateTextureFromFile(const std::string& file);
	SDL_Texture* CreateTextureFromString(const std::string& text, const std::string& fontPath, int ptSize = 12, const SDL_Color& textColor = { 255, 255, 255 });
	TTF_Font* LoadFont(const std::string& file, unsigned int size);

protected:
	void Destroy() override;

private:
	std::string m_DataPath;
	SDL_Texture* CreateTextureFromSurface(SDL_Surface* Surface);
};

