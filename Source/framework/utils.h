#pragma once
#include "Services\ServiceLocator.h"

extern ServiceLocator _serviceLocator;

namespace utils
{
	struct tileInfoBase
	{

	};

	struct test : public tileInfoBase
	{
		test(float _x) { x = _x; };
		float x = 1.f;
	};
}