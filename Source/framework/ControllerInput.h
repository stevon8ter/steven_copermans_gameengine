#pragma once
#include "Input.h"
class ControllerInput final : public Input
{
public:
	ControllerInput(int ID) { m_InputID = ID + 1; };
	~ControllerInput();

	bool IsInputPressed(int input) override;
	bool IsAnyKeyPressed() override;
};

