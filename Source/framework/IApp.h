#ifndef BASE_APPLICATION_H
#define BASE_APPLICATION_H

//-----------------------------------------------------------------
// Includes & Forward Declarations
//-----------------------------------------------------------------


//-----------------------------------------------------------------
// Application Base
//-----------------------------------------------------------------


class IApp
{
public:
	//Constructor & Destructor
	IApp() {};
	virtual ~IApp() = default;

	//App Functions
	virtual void Start() = 0;
	virtual void Update() = 0;

protected:

private:

	//C++ make the class non-copyable
	IApp(const IApp&) {};
	IApp& operator=(const IApp&) {};
};
#endif