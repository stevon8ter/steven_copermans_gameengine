#pragma once
#include "Command.h"
#include <map>
#include "SDL.h"

class Input
{
public:
	Input();
	~Input();

	void HandleInput();
	virtual bool IsInputPressed(int) = 0;

	void AddCommand(std::string inputKey, std::shared_ptr<Command> command);
	void RemoveCommand(std::string inputKey);
	void SetInputDictionary(std::map<std::string, int> dictionary);
	virtual bool IsAnyKeyPressed() = 0;
	void SetEventPointer(SDL_Event* eventPtr) { m_Event = eventPtr; };

	int GetControllerID() { return m_InputID; };
	void ClearCommands();
	void ResetKeys();

protected:
	std::map<int, std::shared_ptr<Command>> m_Commands;
	std::map<std::string, int> m_InputDictionary;
	std::map<int, bool> m_StillHolding;
	int m_InputID = -1;
	SDL_Event* m_Event;
};

