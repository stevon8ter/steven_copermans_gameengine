#pragma once
#include "Scene.h"
#include "Components.h"

class Component;
class GameObject final : std::enable_shared_from_this<GameObject>
{
	// Give SceneService full access to GameObject
	friend class Scene;
public:
	GameObject();
	~GameObject();

	template<typename T>
	inline std::shared_ptr<T> AddComponent(std::shared_ptr<T> component);
	template<typename T>
	inline std::shared_ptr<T> GetComponent();

	void SetTransform(Transform transform) { m_Transform = transform; };
	Transform GetTransform() { return m_Transform; };

private:
	Transform m_Transform;
	std::map<char const * __ptr64, std::shared_ptr<Component>> m_ComponentPtrArr;

	Transform GetNewTransform(Transform componentTransform, float multiplier);

	void Update(float deltaTime);
	void Render();

	void DeleteComponents();
	void Destroy();
};

template<typename T>
inline std::shared_ptr<T> GameObject::AddComponent(std::shared_ptr<T> component)
{
	static_assert(std::is_base_of<Component, T>::value, "T must inherit from Component");
	m_ComponentPtrArr.insert(std::pair<char const * __ptr64, std::shared_ptr<Component>>(typeid(T).name(), component));

	return component;
}

template<typename T>
inline std::shared_ptr<T> GameObject::GetComponent()
{
	static_assert(std::is_base_of<Component, T>::value, "T must inherit from Component");
	if (m_ComponentPtrArr.count(typeid(T).name()))
	{
		return std::static_pointer_cast<T>(m_ComponentPtrArr[typeid(T).name()]);
	}

	return nullptr;
}