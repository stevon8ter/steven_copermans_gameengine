#include "MiniginPCH.h"
#include "Scene.h"
#include "GameObject.h"
#include "Components\ScriptComponent.h"

void Scene::DeleteAllButScript()
{
	auto it = m_GameObjectPtrArr.begin();
	while (it != m_GameObjectPtrArr.end())
	{
		if (!(*it).second->GetComponent<ScriptComponent>())
		{
			m_GameObjectPtrArr.erase(it++);
		}
		else
		{
			++it;
		}
	}
}

std::string Scene::GetName() const
{
	return m_Name;
}

void Scene::Load() const
{
	for (auto gameObject : m_GameObjectPtrArr)
	{
		std::shared_ptr<ScriptComponent> script = gameObject.second->GetComponent<ScriptComponent>();

		if (script)
			script->OnLoad();
	}
}

void Scene::Exit(Scene& nextScene) const
{
	for (auto gameObject : m_GameObjectPtrArr)
	{
		std::shared_ptr<ScriptComponent> script = gameObject.second->GetComponent<ScriptComponent>();

		if (script)
		{
			script->OnExit(nextScene);
		}
			
	}
}

Scene::Scene(const std::string& name) : m_Name(name) {};

void Scene::Destroy()
{
	DeleteGameObjects();
	delete this;
}

void Scene::DeleteGameObjects()
{
	m_GameObjectPtrArr.clear();
}

Scene::~Scene()
{
}

//void dae::Scene::Add(const std::shared_ptr<SceneObject>& object)
//{
//	mObjects.push_back(object);
//}

std::shared_ptr<GameObject> Scene::AddGameObject(std::string objectName, std::shared_ptr<GameObject> gameObject)
{
	m_GameObjectPtrArr.insert({ objectName, gameObject });
	std::cout << "Number of objects in scene: " << m_GameObjectPtrArr.size() << std::endl;
	return gameObject;
}

std::shared_ptr<GameObject> Scene::GetGameObject(std::string objectName)
{
	if (m_GameObjectPtrArr.count(objectName))
	{
		return m_GameObjectPtrArr[objectName];
	}
	return nullptr;
}

void Scene::Update()
{
	//for(auto gameObject : mObjects)
	//{
	//	gameObject->Update();
	//}

	for (auto gameObject : m_GameObjectPtrArr)
	{
		gameObject.second->Update(1.f);
	}
}

void Scene::Render() const
{
	//for (const auto gameObject : mObjects)
	//{
	//	gameObject->Render();
	//}
	for (auto gameObject : m_GameObjectPtrArr)
	{
		gameObject.second->Render();
	}
}

