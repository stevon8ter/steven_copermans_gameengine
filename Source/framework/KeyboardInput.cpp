#include "MiniginPCH.h"
#include "KeyboardInput.h"
#include "SDL.h"

KeyboardInput::~KeyboardInput()
{
}

bool KeyboardInput::IsInputPressed(int input)
{
	//SDL_PumpEvents();
	const Uint8* keyboard = SDL_GetKeyboardState(NULL);

	bool current = keyboard[input];
	if (m_StillHolding[input] && current)
		current = false;
	else
		m_StillHolding[input] = current;

	return current;
}

bool KeyboardInput::IsAnyKeyPressed()
{
	if ((*m_Event).type == SDL_KEYDOWN)
	{
		return true;
	}
	return false;
}
