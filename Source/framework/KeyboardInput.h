#pragma once
#include "Input.h"
class KeyboardInput final : public Input
{
public:
	KeyboardInput() { m_InputID = 0; };
	~KeyboardInput();

	bool IsInputPressed(int input) override;
	bool IsAnyKeyPressed() override;
};

