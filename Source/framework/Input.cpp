#include "MiniginPCH.h"
#include "Input.h"
#include <Xinput.h>
#include "Command.h"

Input::Input()
{
}


Input::~Input()
{
}

void Input::HandleInput()
{
	for (const auto &key : m_Commands)
	{
		if (IsInputPressed(key.first))
		{
			if (key.second)
				key.second->Execute();
		}
		
	}
}

void Input::AddCommand(std::string inputKey, std::shared_ptr<Command> command)
{
	m_Commands.insert(std::pair<int, std::shared_ptr<Command>>(m_InputDictionary[inputKey], std::move(command)));
	m_StillHolding.insert(std::pair<int, bool>(m_InputDictionary[inputKey], false));

	if (IsInputPressed(m_InputDictionary[inputKey]))
		m_StillHolding[m_InputDictionary[inputKey]] = true;
}

void Input::RemoveCommand(std::string inputKey)
{
	m_Commands.erase(m_InputDictionary[inputKey]);
	m_StillHolding.erase(m_InputDictionary[inputKey]);
	m_InputDictionary.erase(inputKey);
}

void Input::SetInputDictionary(std::map<std::string, int> dictionary)
{
	m_InputDictionary = dictionary;
}

void Input::ClearCommands()
{
	m_Commands.clear();
	m_StillHolding.clear();
	m_InputDictionary.clear();
}

void Input::ResetKeys()
{
	std::cout << "trying to reset keys";
	for (auto key : m_InputDictionary)
	{
		m_StillHolding[key.second] = false;
	}
}
