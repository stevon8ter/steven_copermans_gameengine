#pragma once
#include "Services\SceneService.h"
	class GameObject;
	class Scene
	{
		friend class SceneService;
	public:
		//void Add(const std::shared_ptr<SceneObject>& object);
		std::shared_ptr<GameObject> AddGameObject(std::string objectName, std::shared_ptr<GameObject> gameObject);
		std::shared_ptr<GameObject> GetGameObject(std::string objectName);

		void Update();
		void Render() const;

		~Scene();
		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;
		void DeleteAllButScript();

		std::string GetName() const;

	private: 
		void Load() const;
		void Exit(Scene& nextScene) const;

		explicit Scene(const std::string& name);
		void Destroy();
		void DeleteGameObjects();

		std::string m_Name{};
		std::map<std::string, std::shared_ptr<GameObject>> m_GameObjectPtrArr;
	};