#include "MiniginPCH.h"
#include "Services.h"
#include "SDL.h"
#include "Components.h"

DrawingMode TextComponent::_defaultTextDrawingMode = DrawingMode::CORNER;

TextComponent::TextComponent(const std::string & text, const std::string & fontPath, int ptSize, const SDL_Color &textColor)
{
	m_FontPath = fontPath;
	m_PtSize = ptSize;
	m_TextColor = textColor;
	SetText(text);
	m_DrawingMode = DrawingMode::UNDEFINED;
}

TextComponent::~TextComponent()
{
}

void TextComponent::SetText(const std::string &text)
{
	m_Text = text;
	if (m_Texture != nullptr)
		SDL_DestroyTexture(m_Texture);
	m_Texture = _serviceLocator.GetService<ResourceService>()->CreateTextureFromString(text, m_FontPath, m_PtSize, m_TextColor);
}

void TextComponent::Render() const
{
	DrawingMode drawMode = _defaultTextDrawingMode;
	if (m_DrawingMode != DrawingMode::UNDEFINED)
		drawMode = m_DrawingMode;

	auto pos = m_Transform.position;
	if (drawMode == DrawingMode::CENTER)
	{
		pos -= GetSize() / 2.f;
	}

	_serviceLocator.GetService<RenderService>()->RenderTexture(m_Texture, pos.x, pos.y);
}
