#pragma once
struct SDL_Texture;
#include "Components\Component.h"

enum DrawingMode
{
	CORNER,
	CENTER,
	UNDEFINED
};

class TextureComponent : public Component
{
	
public:
	explicit TextureComponent(std::string resource);
	~TextureComponent();

	void SetDrawingMode(DrawingMode mode) { m_DrawingMode = mode; };
	void SetDefaultDrawingMode(DrawingMode mode) { _defaultTextureDrawingMode = mode; };

	glm::vec2 GetSize() const;

protected:
	SDL_Texture* m_Texture = nullptr;
	TextureComponent() {};
	void Render() const override;

private:
	static DrawingMode _defaultTextureDrawingMode;
	DrawingMode m_DrawingMode;
};

