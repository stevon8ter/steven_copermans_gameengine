#pragma once
#include <glm/vec2.hpp>

class GridTileComponent : public Component
{
public:
	GridTileComponent(glm::vec2 tileSize);
	~GridTileComponent();

	template <typename T>
	void SetTileInfo(T* tileInfo);

	template <typename T>
	T GetTileInfo();

private:
	utils::tileInfoBase* m_TileInfo;
	glm::vec2 m_TileSize;
	
};

template<typename T>
inline void GridTileComponent::SetTileInfo(T * tileInfo)
{
	static_assert(std::is_base_of<utils::tileInfoBase, T>::value, "T must inherit from TileInfoBase");
	m_TileInfo = tileInfo;
}

template<typename T>
inline T GridTileComponent::GetTileInfo()
{
	static_assert(std::is_base_of<utils::tileInfoBase, T>::value, "T must inherit from TileInfoBase");
	return *static_cast<T*>(m_TileInfo);
}
