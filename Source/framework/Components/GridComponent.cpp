#include "MiniginPCH.h"
#include "Components.h"
#include <algorithm>
#include <glm\vec2.hpp>

GridComponent::GridComponent(glm::vec2 gridSize, int columns, int rows) :
	m_GridSize{ gridSize },
	m_Rows{ rows },
	m_Columns{ columns }
{
}


GridComponent::~GridComponent()
{
}

std::shared_ptr<GameObject> GridComponent::AddGridTileObject(std::shared_ptr<GameObject> gridObject)
{
	if (gridObject->GetComponent<GridTileComponent>() != nullptr)
	{
		auto gridPosition = GetGridPositionFromWorldPosition(gridObject->GetTransform().position);

		m_GridObjects.push_back(std::pair < glm::vec2, std::shared_ptr<GameObject>>(gridPosition, gridObject));
		return gridObject;
	}
	return nullptr;
}

glm::vec2 GridComponent::GetWorldPositionFromGridPosition(glm::vec2 gridPosition)
{
	glm::vec2 tileSize = m_GridSize / glm::vec2(m_Columns, m_Rows);

	return (tileSize * gridPosition) + tileSize / 2.f;
}

glm::vec2 GridComponent::GetGridPositionFromWorldPosition(glm::vec2 position)
{
	glm::vec2 tileSize = m_GridSize / glm::vec2(m_Columns, m_Rows);
	glm::vec2 floatingGridPos = position / tileSize;
	return glm::vec2(floor(floatingGridPos.x), floor(floatingGridPos.y));
}

std::shared_ptr<GameObject> GridComponent::GetGridTileObjectFromWorldPosition(const glm::vec2 worldPosition)
{
	return GetGridTileObjectFromGridPosition(GetGridPositionFromWorldPosition(worldPosition));
}

std::shared_ptr<GameObject> GridComponent::GetGridTileObjectFromGridPosition(const glm::vec2 gridPosition)
{
	auto compareVec = [&](auto gridTile) {
		glm::vec2 gt = gridTile.first;
		return (gt.x == gridPosition.x && gt.y == gridPosition.y); 
	};
	
	auto it = std::find_if(m_GridObjects.begin(), m_GridObjects.end(), compareVec);
	if (it != m_GridObjects.end())
		return it->second;

	return nullptr;
}
