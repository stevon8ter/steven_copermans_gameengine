#pragma once
#include <glm\vec2.hpp>

class GameObject;
class GridComponent : public Component
{
	struct gridPos
	{
		int x = 0;
		int y = 0;
	};

public:
	GridComponent(glm::vec2 gridSize, int columns, int rows);
	~GridComponent();

	std::shared_ptr<GameObject> AddGridTileObject(std::shared_ptr<GameObject> gridObject);
	glm::vec2 GetWorldPositionFromGridPosition(glm::vec2 gridPosition);
	glm::vec2 GetGridPositionFromWorldPosition(glm::vec2 position);
	std::shared_ptr<GameObject> GetGridTileObjectFromGridPosition(const glm::vec2 gridPosition);
	std::shared_ptr<GameObject> GetGridTileObjectFromWorldPosition(const glm::vec2 worldPosition);

private:
	glm::vec2 m_GridSize;
	int m_Rows;
	int m_Columns;
	std::vector<std::pair < glm::vec2, std::shared_ptr<GameObject>>> m_GridObjects;
};

