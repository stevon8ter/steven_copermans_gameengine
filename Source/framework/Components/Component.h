#pragma once
#include "Transform.h"
#include "GameObject.h"

class Component
{
	// Give Gameobject full access to the component
	friend class GameObject;
public:
	//Constructor & Destructor
	virtual ~Component() = default;

	virtual const Transform& getTransform() const { return m_Transform; };
	virtual void setTransform(Transform transform) { m_Transform = transform; };

protected:
	// Each component has a transform, this is relative to the gameobject it is attached to.
	Transform m_Transform;
	Component() {};

	virtual void Destroy() { delete this; };
	virtual void Render() const {};
	virtual void Update(float) {};

private:
	//C++ make the class non-copyable
	Component(const Component&) {};
	Component& operator=(const Component&) {};
};