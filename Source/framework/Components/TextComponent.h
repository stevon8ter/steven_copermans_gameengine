#pragma once
#include <SDL.h>

class TextComponent final : public TextureComponent
{
public:
	explicit TextComponent(const std::string& text, const std::string& fontPath, int ptSize = 36, const SDL_Color& textColor = { 255, 255, 255 });
	~TextComponent();

	const std::string &GetText() const { return m_Text; };
	void SetText(const std::string& text);
	void SetFontSize(const int size) { m_PtSize = size; };
	void SetDrawingMode(DrawingMode mode) { m_DrawingMode = mode; };
	void SetDefaultDrawingMode(DrawingMode mode) { _defaultTextDrawingMode = mode; };

protected:
	void Render() const override;

private:
	std::string m_Text;
	std::string m_FontPath;
	int m_PtSize;
	SDL_Color m_TextColor;
	static DrawingMode _defaultTextDrawingMode;
	DrawingMode m_DrawingMode;
};