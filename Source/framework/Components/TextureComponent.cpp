#include "MiniginPCH.h"
#include "Components.h"
#include "Services.h"
#include <SDL.h>

DrawingMode TextureComponent::_defaultTextureDrawingMode = DrawingMode::CORNER;

TextureComponent::TextureComponent(std::string resource)
{
	m_Texture = _serviceLocator.GetService<ResourceService>()->CreateTextureFromFile(resource);
	m_DrawingMode = DrawingMode::UNDEFINED;
}

TextureComponent::~TextureComponent()
{
	SDL_DestroyTexture(m_Texture);
}

void TextureComponent::Render() const
{
	DrawingMode drawMode = _defaultTextureDrawingMode;
	if (m_DrawingMode != DrawingMode::UNDEFINED)
		drawMode = m_DrawingMode;

	auto pos = m_Transform.position;
	if (drawMode == DrawingMode::CENTER)
	{
		pos -= GetSize() / 2.f;
	}
	
	_serviceLocator.GetService<RenderService>()->RenderTexture(m_Texture, pos.x, pos.y);
}

glm::vec2 TextureComponent::GetSize() const
{
	int width, height;
	SDL_QueryTexture(m_Texture, NULL, NULL, &width, &height);

	return glm::vec2(width, height);
}