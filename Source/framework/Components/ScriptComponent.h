#pragma once

class Scene;
class ScriptComponent : public Component
{
public:
	virtual ~ScriptComponent() = default;

	virtual void Update(float) {};
	virtual void Render() const {};
	virtual void OnLoad() {};
	virtual void OnExit(Scene&) {};

protected:
	ScriptComponent() {};
};

