#pragma once

#include "Services\InputService.h"
#include "Services\RenderService.h"
#include "Services\ResourceService.h"
#include "Services\SceneService.h"
#include "Services\ServiceLocator.h"
#include "Services\Service.h"