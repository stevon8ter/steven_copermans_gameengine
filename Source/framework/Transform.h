#pragma once
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec2.hpp>
#pragma warning(pop)

struct Transform {
	glm::vec2 position;
	float rotation;
	glm::vec2 scale{ 1.f };

	void SetPosition(float x, float y) { position.x = x; position.y = y; };
	void SetRotation(float r) { rotation = r; };
	void SetScale(float x, float y) { scale.x = x; scale.y = y; };

	void SetPosition(glm::vec2 pos) { position = pos; };
	void SetScale(glm::vec2 s) { scale = s; };

	Transform& operator+=(Transform transform)
	{
		this->position += transform.position;
		this->rotation += transform.rotation;

		return *this;
	}

	Transform operator*(float multiplier)
	{
		Transform temp;
		temp.scale = this->scale;
		temp.position = this->position * multiplier;
		temp.rotation = this->rotation * multiplier;
		
		return temp;
	}
};