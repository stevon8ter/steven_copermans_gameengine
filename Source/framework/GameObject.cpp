#include "MiniginPCH.h"
#include "GameObject.h"
#include "Components.h"
#include "Services.h"
#include <SDL.h>

GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

Transform GameObject::GetNewTransform(Transform componentTransform, float multiplier)
{
	componentTransform += (this->m_Transform * multiplier);

	return componentTransform;
}

void GameObject::Update(float deltaTime)
{
	for (std::pair<char const * __ptr64, std::shared_ptr<Component>> pair : m_ComponentPtrArr)
	{
		pair.second->setTransform(GetNewTransform(pair.second->getTransform(), 1.f));
		pair.second->Update(deltaTime);
		pair.second->setTransform(GetNewTransform(pair.second->getTransform(), -1.f));
	}
}

void GameObject::Render()
{
	for (std::pair<char const * __ptr64, std::shared_ptr<Component>> pair : m_ComponentPtrArr)
	{
		SDL_RenderSetScale(_serviceLocator.GetService<RenderService>()->GetSDLRenderer(),
			pair.second->getTransform().scale.x * this->m_Transform.scale.x, pair.second->getTransform().scale.y * this->m_Transform.scale.y);
		pair.second->setTransform(GetNewTransform(pair.second->getTransform(), 1.f));
		//std::cout << "x pos: " << pair.second->getTransform().position.x << std::endl;
		pair.second->Render();
		pair.second->setTransform(GetNewTransform(pair.second->getTransform(), -1.f));
		SDL_RenderSetScale(_serviceLocator.GetService<RenderService>()->GetSDLRenderer(),
			1.f, 1.f);
	}
}

void GameObject::DeleteComponents()
{
	for (std::pair<char const * __ptr64, std::shared_ptr<Component>> pair : m_ComponentPtrArr)
	{
		pair.second->Destroy();
	}
}

void GameObject::Destroy()
{
	DeleteComponents();
	//delete this;
}
