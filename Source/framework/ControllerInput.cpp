#include "MiniginPCH.h"
#include "ControllerInput.h"
#include <Xinput.h>

ControllerInput::~ControllerInput()
{
}

bool ControllerInput::IsInputPressed(int input)
{
	XINPUT_STATE currentState{};
	ZeroMemory(&currentState, sizeof(XINPUT_STATE));
	XInputGetState(m_InputID - 1, &currentState);

	bool current = currentState.Gamepad.wButtons & input;
	if (m_StillHolding[input] && current)
		current = false;
	else
		m_StillHolding[input] = current;

	return current;
}

bool ControllerInput::IsAnyKeyPressed()
{
	XINPUT_STATE currentState{};
	ZeroMemory(&currentState, sizeof(XINPUT_STATE));
	XInputGetState(m_InputID - 1, &currentState);
	return currentState.Gamepad.wButtons > 0;
}
