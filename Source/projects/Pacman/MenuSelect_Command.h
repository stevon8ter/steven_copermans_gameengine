#pragma once
#include "Command.h"

class GameObject;
class MenuSelect_Command final : public Command
{
public:
	MenuSelect_Command(std::vector<std::shared_ptr<GameObject>> menuItems, std::vector<std::string> sceneSelections);
	~MenuSelect_Command();

	void Execute() override;

private:
	std::vector<std::shared_ptr<GameObject>> m_MenuItems;
	std::vector<std::string> m_Scenes;
	static int index;
};

