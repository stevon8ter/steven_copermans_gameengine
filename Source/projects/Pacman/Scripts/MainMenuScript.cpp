#include "MiniginPCH.h"
#include "MainMenuScript.h"
#include "Services.h"
#include "Pacman\MenuMove_Command.h"
#include "Pacman\MenuSelect_Command.h"
#include "ControllerInput.h"
#include "KeyboardInput.h"
#include "CharacterMenuScript.h"

MainMenuScript::MainMenuScript()
{
}


MainMenuScript::~MainMenuScript()
{
}

void MainMenuScript::OnLoad()
{
	//Local transform to be used for setting positions
	Transform transform;

#pragma region Background

	auto& scene = _serviceLocator.GetService<SceneService>()->GetActiveScene();

	//Background
	std::shared_ptr<GameObject> gameObject = std::make_shared<GameObject>();
	gameObject->AddComponent<TextureComponent>(std::make_shared<TextureComponent>("background.jpg"));

	scene.AddGameObject("Background", gameObject);
#pragma endregion

#pragma region Title
	//Title
	gameObject = std::make_shared<GameObject>();
	auto textComponent = gameObject->AddComponent<TextComponent>(std::make_shared<TextComponent>("PACMAN", "ARCADE_I.ttf", 120, SDL_Color{ 255, 255, 0, 255 }));
	textComponent->SetDefaultDrawingMode(DrawingMode::CENTER);

	transform.position.x = 500;
	transform.position.y = 115;
	gameObject->SetTransform(transform);

	scene.AddGameObject("Title", gameObject);
#pragma endregion

#pragma region MultiplayerOption
	//Multiplayer
	std::shared_ptr<GameObject> Multiplayer = std::make_shared<GameObject>();
	textComponent = Multiplayer->AddComponent<TextComponent>(std::make_shared<TextComponent>("Multiplayer", "ARCADE_N.ttf", 50, SDL_Color{ 255, 255, 255, 255 }));

	transform.position.x = 500;
	transform.position.y = 500;
	Multiplayer->SetTransform(transform);

	scene.AddGameObject("Multiplayer", Multiplayer);
#pragma endregion

#pragma region Singleplayer
	//Singleplayer
	std::shared_ptr<GameObject> Singleplayer = std::make_shared<GameObject>();
	textComponent = Singleplayer->AddComponent<TextComponent>(std::make_shared<TextComponent>("Singleplayer", "ARCADE_N.ttf", 50, SDL_Color{ 255, 255, 255, 255 }));

	transform.position.x = 500;
	transform.position.y = 350;
	Singleplayer->SetTransform(transform);

	scene.AddGameObject("Singleplayer", Singleplayer);
#pragma endregion

#pragma region Pointer
	//PointerLeft
	gameObject = std::make_shared<GameObject>();
	textComponent = gameObject->AddComponent<TextComponent>(std::make_shared<TextComponent>(">", "ARCADE_N.ttf", 50, SDL_Color{ 255, 255, 255, 255 }));

	transform.position.x = textComponent->GetSize().x + 75;
	gameObject->SetTransform(transform);

	scene.AddGameObject("LeftPointer", gameObject);

	//PointerRight
	gameObject = std::make_shared<GameObject>();
	textComponent = gameObject->AddComponent<TextComponent>(std::make_shared<TextComponent>("<", "ARCADE_N.ttf", 50, SDL_Color{ 255, 255, 255, 255 }));

	transform.position.x = 1000 - textComponent->GetSize().x - 75;
	gameObject->SetTransform(transform);

	scene.AddGameObject("RightPointer", gameObject);
#pragma endregion

	//std::shared_ptr<GameObject> player1Object = std::make_shared<GameObject>();
	//player1Object->AddComponent


	// Controller input, adding commands to move pointers
	std::vector<std::shared_ptr<GameObject>> menuItems = { Singleplayer, Multiplayer };

	std::shared_ptr<MenuMove_Command> moveDownCommand = std::make_shared<MenuMove_Command>(1, menuItems);
	std::shared_ptr<MenuMove_Command> moveUpCommand = std::make_shared<MenuMove_Command>(-1, menuItems);
	std::shared_ptr<MenuSelect_Command> selectCommand = std::make_shared<MenuSelect_Command>(menuItems, std::vector<std::string>{"Pacman_Game", "Character_Menu"});

	std::map<std::string, int> controllerDictionary = { { "UP", XINPUT_GAMEPAD_DPAD_UP },{ "DOWN", XINPUT_GAMEPAD_DPAD_DOWN },{ "ENTER", XINPUT_GAMEPAD_A } };
	std::map<std::string, int> keyboardDictionary = { { "UP", SDL_SCANCODE_UP },{ "DOWN", SDL_SCANCODE_DOWN },{ "ENTER", SDL_SCANCODE_SPACE } };
	for (int i = 0; i < XUSER_MAX_COUNT; ++i)
	{
		auto input = std::make_shared<ControllerInput>(i);
		input->SetInputDictionary(controllerDictionary);

		input->AddCommand("UP", moveDownCommand);
		input->AddCommand("DOWN", moveUpCommand);
		input->AddCommand("ENTER", selectCommand);

		_serviceLocator.GetService<InputService>()->AddInput(input);
	}

	auto input = std::make_shared<KeyboardInput>();
	input->SetInputDictionary(keyboardDictionary);

	input->AddCommand("UP", moveDownCommand);
	input->AddCommand("DOWN", moveUpCommand);
	input->AddCommand("ENTER", selectCommand);

	_serviceLocator.GetService<InputService>()->AddInput(input);
}

void MainMenuScript::Update(float)
{
	auto inputService = _serviceLocator.GetService<InputService>();
	std::vector<std::shared_ptr<Input>> inputs = inputService->GetInputs();
	int ID = -1;

	if (player1Input)
		ID = player1Input->GetControllerID();
	else
	{
		for (auto input : inputs)
		{
			if (input->IsAnyKeyPressed() && !player1Input)
			{
				ID = input->GetControllerID();
				player1Input = input;

				for (int i = 0; i < 5; ++i)
				{
					if (i != ID)
						inputService->RemoveInput(i);
				}

				break;
			}
		}
	}

	

	//if (player1Input)
	//{
	//	for (int i = 0; i < 5; ++i)
	//	{
	//		if (i != ID)
	//			inputService->RemoveInput(i);
	//	}
	//}
}

void MainMenuScript::OnExit(Scene & nextScene)
{
	auto inputs = _serviceLocator.GetService<InputService>()->GetInputs();

	for (auto input : inputs)
	{
		input->ClearCommands();
	}

	auto& currentScene = _serviceLocator.GetService<SceneService>()->GetActiveScene();

	if (nextScene.GetName() == "Character_Menu")
	{
		std::shared_ptr<CharacterMenuScript> script = std::static_pointer_cast<CharacterMenuScript>(nextScene.GetGameObject("Script")->GetComponent<ScriptComponent>());
		script->SetPlayer1Input(player1Input);

		nextScene.AddGameObject("Background", currentScene.GetGameObject("Background"));

		auto gameObject = currentScene.GetGameObject("Title");
		gameObject->GetComponent<TextComponent>()->SetFontSize(100);
		gameObject->GetComponent<TextComponent>()->SetText("Player 2");
		nextScene.AddGameObject("Title", gameObject);
		
		gameObject = currentScene.GetGameObject("Singleplayer");
		gameObject->GetComponent<TextComponent>()->SetText("Miss Pacman");
		nextScene.AddGameObject("Miss_Pacman", gameObject);
		
		Transform transform = gameObject->GetTransform();
		
		gameObject = currentScene.GetGameObject("LeftPointer");
		transform.position.x = gameObject->GetTransform().position.x;
		gameObject->SetTransform(transform);
		nextScene.AddGameObject("LeftPointer", gameObject);
		
		gameObject = currentScene.GetGameObject("RightPointer");
		transform.position.x = gameObject->GetTransform().position.x;
		gameObject->SetTransform(transform);
		nextScene.AddGameObject("RightPointer", gameObject);

		gameObject = currentScene.GetGameObject("Multiplayer");
		gameObject->GetComponent<TextComponent>()->SetText("Ghost");
		nextScene.AddGameObject("Ghost", gameObject);

		currentScene.DeleteAllButScript();
	}
}
