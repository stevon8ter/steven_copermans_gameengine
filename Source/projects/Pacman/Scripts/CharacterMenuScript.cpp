#include "MiniginPCH.h"
#include "CharacterMenuScript.h"
#include "Services.h"
#include "Pacman\MenuMove_Command.h"
#include "Pacman\MenuSelect_Command.h"
#include "ControllerInput.h"
#include "KeyboardInput.h"

CharacterMenuScript::CharacterMenuScript()
{
}


CharacterMenuScript::~CharacterMenuScript()
{
}

void CharacterMenuScript::OnLoad()
{
	//_serviceLocator.GetService<SceneService>()->SetActiveScene("Main_Menu");
	std::cout << "Player 1 input: " << player1Input->GetControllerID() << std::endl;

	auto mssPacman = _serviceLocator.GetService<SceneService>()->GetActiveScene().GetGameObject("Miss_Pacman");
	auto ghost = _serviceLocator.GetService<SceneService>()->GetActiveScene().GetGameObject("Ghost");

	std::vector<std::shared_ptr<GameObject>> menuItems = { mssPacman, ghost };

	std::shared_ptr<MenuMove_Command> moveDownCommand = std::make_shared<MenuMove_Command>(1, menuItems);
	std::shared_ptr<MenuMove_Command> moveUpCommand = std::make_shared<MenuMove_Command>(-1, menuItems);
	std::shared_ptr<MenuSelect_Command> selectCommand = std::make_shared<MenuSelect_Command>(menuItems, std::vector<std::string>{"Pacman_Game", "Pacman_Game"});

	std::map<std::string, int> controllerDictionary = { { "UP", XINPUT_GAMEPAD_DPAD_UP },{ "DOWN", XINPUT_GAMEPAD_DPAD_DOWN },{ "ENTER", XINPUT_GAMEPAD_A } };
	std::map<std::string, int> keyboardDictionary = { { "UP", SDL_SCANCODE_UP },{ "DOWN", SDL_SCANCODE_DOWN },{ "ENTER", SDL_SCANCODE_SPACE } };
	for (int i = 0; i < XUSER_MAX_COUNT; ++i)
	{
		if (player1Input->GetControllerID() - 1 != i)
		{
			auto input = std::make_shared<ControllerInput>(i);
			input->SetInputDictionary(controllerDictionary);

			input->AddCommand("UP", moveDownCommand);
			input->AddCommand("DOWN", moveUpCommand);
			input->AddCommand("ENTER", selectCommand);

			_serviceLocator.GetService<InputService>()->AddInput(input);
		}
	}

	auto input = std::make_shared<KeyboardInput>();
	if (player1Input->GetControllerID() == 0)
	{
		input = std::static_pointer_cast<KeyboardInput>(player1Input);
	}
	input->SetInputDictionary(keyboardDictionary);

	input->AddCommand("UP", moveDownCommand);
	input->AddCommand("DOWN", moveUpCommand);
	input->AddCommand("ENTER", selectCommand);

	if (player1Input->GetControllerID() != 0)
	{
		_serviceLocator.GetService<InputService>()->AddInput(input);
	}
		
	
	std::cout << _serviceLocator.GetService<InputService>()->GetInputs().size() << std::endl;
}

void CharacterMenuScript::Update(float)
{
	auto inputService = _serviceLocator.GetService<InputService>();
	std::vector<std::shared_ptr<Input>> inputs = inputService->GetInputs();
	int ID = -1;

	if (player2Input)
		ID = player2Input->GetControllerID();

	for (auto input : inputs)
	{
		if (input->IsAnyKeyPressed() && !player2Input && input->GetControllerID() != player1Input->GetControllerID())
		{
			ID = input->GetControllerID();
			player2Input = input;
		}
	}

	if (player2Input)
	{
		for (int i = 0; i < 5; ++i)
		{
			if (i != ID && i != player1Input->GetControllerID())
				inputService->RemoveInput(i);
		}
	}
}

void CharacterMenuScript::OnExit(Scene &)
{
	auto inputs = _serviceLocator.GetService<InputService>()->GetInputs();

	for (auto input : inputs)
	{
		input->ClearCommands();
	}

	std::cout << "Amount of inputs: " << inputs.size() << std::endl;

	auto sceneService = _serviceLocator.GetService<SceneService>();
	sceneService->GetActiveScene().DeleteAllButScript();
	//auto& currentScene = sceneService->GetActiveScene();
}
