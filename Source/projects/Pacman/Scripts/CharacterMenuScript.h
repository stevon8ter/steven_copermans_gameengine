#pragma once
#include "Input.h"
#include "Components.h"

class CharacterMenuScript : public ScriptComponent
{
public:
	CharacterMenuScript();
	~CharacterMenuScript();

	void OnLoad() override;
	void Update(float) override;
	void OnExit(Scene& nextScene) override;

	void SetPlayer1Input(std::shared_ptr<Input> input) { player1Input = input; };

private:
	std::shared_ptr<Input> player1Input;
	std::shared_ptr<Input> player2Input;
};

