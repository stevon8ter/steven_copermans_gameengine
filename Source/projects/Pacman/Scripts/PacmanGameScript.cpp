#include "MiniginPCH.h"
#include "PacmanGameScript.h"
#include "Services.h"
#include "ControllerInput.h"
#include "KeyboardInput.h"
#include <glm\vec2.hpp>

PacmanGameScript::PacmanGameScript()
{
}


PacmanGameScript::~PacmanGameScript()
{
}

void PacmanGameScript::OnLoad()
{
	//Local transform to be used for setting positions
	Transform transform;

	auto& scene = _serviceLocator.GetService<SceneService>()->GetActiveScene();

	//Grid
	auto gameObject = std::make_shared<GameObject>();
	auto grid = std::make_shared<GridComponent>(glm::vec2(1000, 750), 20, 15);
	gameObject->AddComponent(grid);

	scene.AddGameObject("Grid", gameObject);

	for (int i = 0; i < 20; ++i)
	{
		transform.position.x = float(i * 50);
		for (int j = 0; j < 15; ++j)
		{
			transform.position.y = float(j * 50);
			auto gridTile = std::make_shared<GameObject>();
			gridTile->AddComponent<TextureComponent>(std::make_shared<TextureComponent>("Tile.png"));
			gridTile->AddComponent<GridTileComponent>(std::make_shared<GridTileComponent>(glm::vec2(25,25)));
			gridTile->SetTransform(transform);
			scene.AddGameObject(std::to_string(i) + "Tile" + std::to_string(j), gridTile);
			grid->AddGridTileObject(gridTile);
			
		}
	}
	

}

void PacmanGameScript::Update(float)
{
}
