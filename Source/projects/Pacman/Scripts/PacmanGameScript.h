#pragma once
#include "Input.h"
#include "Components.h"

class PacmanGameScript :
	public ScriptComponent
{
public:
	PacmanGameScript();
	~PacmanGameScript();

	void SetPlayer1Input(std::shared_ptr<Input> input) { player1Input = input; };
	void SetPlayer2Input(std::shared_ptr<Input> input) { player2Input = input; };

	void OnLoad() override;
	void Update(float) override;

private:
	std::shared_ptr<Input> player1Input;
	std::shared_ptr<Input> player2Input;
};

