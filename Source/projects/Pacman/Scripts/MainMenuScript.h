#pragma once
#include "Input.h"
#include "Components.h"

class MainMenuScript : public ScriptComponent
{
public:
	MainMenuScript();
	~MainMenuScript();

	void OnLoad() override;
	void Update(float) override;
	void OnExit(Scene& nextScene) override;

private:
	std::shared_ptr<Input> player1Input;
};

