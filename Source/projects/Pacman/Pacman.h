#pragma once
#include "IApp.h"
#include "Input.h"

class Pacman final : public IApp
{
public:
	Pacman();
	~Pacman();

	void Start() override;
	void Update() override;

private:
	void Setup_MainMenu();
	void Setup_CharacterSelect();
	void Setup_Level();

};