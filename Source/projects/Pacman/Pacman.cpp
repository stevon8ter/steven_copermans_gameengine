#include "MiniginPCH.h"
#include "Pacman.h"
#include "Services.h"
#include "Scene.h"

#include "Components.h"

#include "Scripts\MainMenuScript.h"
#include "Scripts\CharacterMenuScript.h"
#include "Scripts\PacmanGameScript.h"

Pacman::Pacman()
{
}


Pacman::~Pacman()
{
}

void Pacman::Start() {
	Setup_MainMenu();
	Setup_CharacterSelect();
	Setup_Level();
}

void Pacman::Update()
{
	std::string currentScene = _serviceLocator.GetService<SceneService>()->GetActiveScene().GetName();

	if (currentScene == "Main_Menu")
	{

	}
	else if (currentScene == "Character_Selection") {

	}
	else if (currentScene == "Pacman_Game") {

	}
}

void Pacman::Setup_MainMenu()
{
	auto& menuScene = _serviceLocator.GetService<SceneService>()->CreateScene("Main_Menu");

	std::shared_ptr<GameObject> gameObject = std::make_shared<GameObject>();
	gameObject->AddComponent<ScriptComponent>(std::make_shared<MainMenuScript>());

	menuScene.AddGameObject("Script", gameObject);
	_serviceLocator.GetService<SceneService>()->SetActiveScene("Main_Menu");
}

void Pacman::Setup_CharacterSelect()
{
	auto& scene = _serviceLocator.GetService<SceneService>()->CreateScene("Character_Menu");

	std::shared_ptr<GameObject> gameObject = std::make_shared<GameObject>();
	gameObject->AddComponent<ScriptComponent>(std::make_shared<CharacterMenuScript>());

	scene.AddGameObject("Script", gameObject);
}

void Pacman::Setup_Level()
{
	auto& scene = _serviceLocator.GetService<SceneService>()->CreateScene("Pacman_Game");

	std::shared_ptr<GameObject> gameObject = std::make_shared<GameObject>();
	gameObject->AddComponent<ScriptComponent>(std::make_shared<PacmanGameScript>());

	scene.AddGameObject("Script", gameObject);
}
