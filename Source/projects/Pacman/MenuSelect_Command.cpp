#include "MiniginPCH.h"
#include "MenuSelect_Command.h"

#include "Services.h"
#include "GameObject.h"
#include "Transform.h"
#include "Components.h"

MenuSelect_Command::MenuSelect_Command(std::vector<std::shared_ptr<GameObject>> menuItems, std::vector<std::string> sceneSelections)
{
	m_MenuItems = menuItems;
	m_Scenes = sceneSelections;
}


MenuSelect_Command::~MenuSelect_Command()
{
}

void MenuSelect_Command::Execute()
{
	auto leftPointer = _serviceLocator.GetService<SceneService>()->GetActiveScene().GetGameObject("LeftPointer");
	auto pointerY = leftPointer->GetTransform().position.y;

	for (int i = 0; i < m_MenuItems.size(); ++i)
	{
		if (m_MenuItems[i]->GetTransform().position.y == pointerY)
		{
			_serviceLocator.GetService<SceneService>()->SetActiveScene(m_Scenes[i]);
		}
	}

	//for (auto item : m_MenuItems)
	//{
	//	if (item->GetTransform().position.y == pointerY)
	//	{
	//		if (item->GetComponent<TextComponent>()->GetText() == "Singleplayer")
	//		{
	//			_serviceLocator.GetService<SceneService>()->SetActiveScene("Pacman_Game");
	//		}
	//		else if (item->GetComponent<TextComponent>()->GetText() == "Multiplayer")
	//		{
	//			_serviceLocator.GetService<SceneService>()->SetActiveScene("Character_Menu");
	//		}
	//	}
	//}
}
