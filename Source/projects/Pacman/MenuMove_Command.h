#pragma once
#include "Command.h"

class GameObject;
class MenuMove_Command final : public Command
{
public:
	MenuMove_Command(int direction, std::vector<std::shared_ptr<GameObject>> menuItems);
	~MenuMove_Command() {};

	void Execute() override;

private:
	int moveDirection;
	std::vector<std::shared_ptr<GameObject>> m_MenuItems;
	static int index;
};

