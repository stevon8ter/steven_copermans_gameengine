#include "MiniginPCH.h"
#include "MenuMove_Command.h"

#include "Services.h"
#include "GameObject.h"
#include "Transform.h"
#include "Components.h"

int MenuMove_Command::index = 0;

MenuMove_Command::MenuMove_Command(int direction, std::vector<std::shared_ptr<GameObject>> menuItems)
{
	moveDirection = direction;
	m_MenuItems = menuItems;
}

void MenuMove_Command::Execute()
{
	auto leftPointer = _serviceLocator.GetService<SceneService>()->GetActiveScene().GetGameObject("LeftPointer");
	auto rightPointer = _serviceLocator.GetService<SceneService>()->GetActiveScene().GetGameObject("RightPointer");

	auto txt = leftPointer->GetComponent<TextComponent>();

	index += moveDirection;

	if (index < 0)
		index = (int)m_MenuItems.size() - 1;

	if (index >= (int)m_MenuItems.size())
		index = 0;

	Transform transform;
	transform.position.x = leftPointer->GetTransform().position.x;
	transform.position.y = m_MenuItems[index]->GetTransform().position.y;

	leftPointer->SetTransform(transform);

	transform.position.x = rightPointer->GetTransform().position.x;
	rightPointer->SetTransform(transform);
}
